FROM tomcat:jre8

RUN apt-get update && apt-get install -y \
    git \
    maven \
    openjdk-8-jdk \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt

# Install WorldWind Server kit
RUN git clone --branch v0.1.0 https://github.com/NASAWorldWind/WorldWindServerKit.git \
    && cd /opt/WorldWindServerKit \
    && mvn clean install \
    && cp worldwind-geoserver/target/worldwind-geoserver.war $CATALINA_HOME/webapps \
    && cd /opt \
    && rm -rf /opt/WorldWindServerKit

# Add tomcat user for management
RUN sed -i 's|<\/tomcat-users>||' $CATALINA_HOME/conf/tomcat-users.xml \
    && echo '  <role rolename="manager-gui"/>' >> $CATALINA_HOME/conf/tomcat-users.xml \
    && echo '  <user username="admin" password="" roles="manager-gui"/>' >> $CATALINA_HOME/conf/tomcat-users.xml \
    && echo '</tomcat-users>' >> $CATALINA_HOME/conf/tomcat-users.xml

# Tomcat remote debugging
ENV JPDA_ADDRESS=9999
ENV JPDA_TRANSPORT=dt_socket    
ENV CATALINA_OPTS="$CATALINA_OPTS -server -Xmx1024m -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=9999"

EXPOSE 8080
EXPOSE 9999

# In order to create the necessary files and folders under
# $CATALINA_HOME/webapps/worldwind-geoserver/WEB-INF/lib, we have to run the
# server once
RUN catalina.sh start
