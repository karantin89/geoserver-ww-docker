## GeoServer WorldWind Server image in Docker

### To run Worldwind-geoserver:

1. Pull or build the docker image:
    * To pull: `docker login registry.gitlab.com && docker pull registry.gitlab.com/gitlabrgi/geoserver-ww-docker -t geoserver-ww-docker`
    * To build: `docker build gitlab.com/gitlabrgi/geoserver-ww-docker -t geoserver-ww-docker`
2. Run the server: `docker run -p 80:8080 geoserver-ww-docker`

### To gain shell access to Worldwind-geoserver:

1. Run interactively: `docker run -it --entrypoint /bin/bash -p 80:8080 geoserver-ww-docker`
