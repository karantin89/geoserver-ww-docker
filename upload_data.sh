#!/bin/bash
# Arguments
# $1 Path to file on local machine
# $2 Name of container
# $3 Name of file
docker cp $1 $2:/usr/local/tomcat/$3
# Graphhopper will fail with "To avoid reading partial data we need to obtain the read lock but it failed." if it cannot have write access. This needs to be fixed.
docker exec $2 chmod 777 /usr/local/tomcat/$3